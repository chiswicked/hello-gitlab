[![Pipeline Status](https://gitlab.com/chiswicked/hello-gitlab/badges/master/pipeline.svg)](https://gitlab.com/chiswicked/hello-gitlab/pipelines)
[![Coverage Report](https://gitlab.com/chiswicked/hello-gitlab/badges/master/coverage.svg)](https://gitlab.com/chiswicked/hello-gitlab/commits/master)
[![Go Report Card](https://goreportcard.com/badge/gitlab.com/chiswicked/hello-gitlab)](https://goreportcard.com/report/gitlab.com/chiswicked/hello-gitlab)
[![License MIT](https://img.shields.io/badge/License-MIT-brightgreen.svg)](https://img.shields.io/badge/License-MIT-brightgreen.svg)