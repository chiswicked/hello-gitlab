# Base image
FROM golang:1.13.1-alpine3.10 AS build

RUN apk update && apk add --no-cache git gcc make musl-dev

ARG PROJECT_NAMESPACE
ARG PROJECT_NAME
ENV SRC_PATH=${GOPATH}/src/gitlab.com/${PROJECT_NAMESPACE}/${PROJECT_NAME}

ADD . ${SRC_PATH}
WORKDIR ${SRC_PATH}

RUN make build
RUN mv build/${PROJECT_NAME} /${PROJECT_NAME}

# Production image
FROM alpine:3.10

ARG PROJECT_NAME
ENV APP=${PROJECT_NAME}

RUN mkdir /app
COPY --from=build /${PROJECT_NAME} /app/${PROJECT_NAME}
ENTRYPOINT exec /app/${APP}
