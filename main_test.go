package main

import (
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestHandler(t *testing.T) {
	tt := []struct {
		name    string
		pattern string
		status  int
		res     string
	}{
		{
			name:    "root without /",
			pattern: "",
			status:  http.StatusOK,
			res:     "Hello from ",
		},
		{
			name:    "root with /",
			pattern: "/",
			status:  http.StatusOK,
			res:     "Hello from /",
		},
		{
			name:    "/something",
			pattern: "/something",
			status:  http.StatusOK,
			res:     "Hello from /something",
		},
		{
			name:    "/something/",
			pattern: "/something/",
			status:  http.StatusOK,
			res:     "Hello from /something/",
		},
		{
			name:    "/something/else",
			pattern: "/something/else",
			status:  http.StatusOK,
			res:     "Hello from /something/else",
		},
	}

	for _, tc := range tt {
		t.Run(tc.name, func(t *testing.T) {

			r, err := http.NewRequest("GET", "http://localhost:8080"+tc.pattern, nil)

			if err != nil {
				t.Fatalf("could not create request: %v", err)
			}

			rec := httptest.NewRecorder()
			handler(rec, r)

			res := rec.Result()
			defer res.Body.Close()

			if res.StatusCode != tc.status {
				t.Errorf("expected: %v; got %v", tc.status, res.StatusCode)
			}

			b, err := ioutil.ReadAll(res.Body)
			if err != nil {
				t.Fatalf("could not read response %v", err)
			}

			if resBody := string(b); resBody != tc.res {
				t.Errorf("expected: %+q; got %+q", tc.res, resBody)
			}
		})
	}
}

func TestHelloMsg(t *testing.T) {
	expected := "Hello from world"
	if got := helloMsg("world"); got != expected {
		t.Errorf("expected: %+q; got: %+q", expected, got)
	}
}
