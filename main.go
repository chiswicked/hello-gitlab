package main

import (
	"fmt"
	"net/http"

	log "github.com/sirupsen/logrus"
)

func main() {
	log.Info("starting application")
	http.HandleFunc("/", handler)
	err := http.ListenAndServe(":8080", nil)
	if err != nil {
		log.Fatalf("terminating application: %v", err)
	}
}

func handler(w http.ResponseWriter, r *http.Request) {
	fmt.Fprint(w, helloMsg(r.URL.Path))
}

func helloMsg(msg string) string {
	return fmt.Sprintf("Hello from %s", msg)
}
